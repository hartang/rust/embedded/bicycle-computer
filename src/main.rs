#![no_std]
#![no_main]

use embedded_graphics::{
    mono_font::{ascii::FONT_6X10, MonoTextStyle},
    pixelcolor::BinaryColor::On as Black,
    prelude::*,
    text::{self, Text},
};
use epd_waveshare::{epd2in9::*, prelude::*};
use esp32c3_hal::{
    clock::{ClockControl, CpuClock},
    gpio::IO,
    peripherals::Peripherals,
    prelude::*,
    spi::{Spi, SpiMode},
    timer::TimerGroup,
    Delay, Rtc,
};
use esp_backtrace as _;
use esp_println::println;
use nb::block;

#[entry]
fn main() -> ! {
    let peripherals = Peripherals::take();
    let mut system = peripherals.SYSTEM.split();
    let clocks = ClockControl::configure(system.clock_control, CpuClock::Clock160MHz).freeze();

    // Disable the RTC and TIMG watchdog timers
    let mut rtc = Rtc::new(peripherals.RTC_CNTL);
    rtc.swd.disable();
    rtc.rwdt.disable();

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let mut wdt0 = timer_group0.wdt;
    wdt0.disable();

    let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
    let mut wdt1 = timer_group1.wdt;
    wdt1.disable();

    let mut timer0 = timer_group0.timer0;
    timer0.start(1u64.secs());

    println!("esp32 clock setup done");

    // E-paper display
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let mut spi = Spi::new_no_cs_no_miso(
        peripherals.SPI2,
        io.pins.gpio6,
        io.pins.gpio7,
        5u32.MHz(),
        SpiMode::Mode0,
        &mut system.peripheral_clock_control,
        &clocks,
    );
    let mut delay = Delay::new(&clocks);

    let epd = Epd2in9::new(
        &mut spi,
        io.pins.gpio5.into_push_pull_output(), // CS
        io.pins.gpio4.into_floating_input(),   // BUSY
        io.pins.gpio3.into_push_pull_output(), // DC
        io.pins.gpio2.into_push_pull_output(), // RST
        &mut delay,
    );
    let mut display = Display2in9::default();

    Text::with_alignment(
        "Hallo Andi",
        display.bounding_box().center(),
        MonoTextStyle::new(&FONT_6X10, Black),
        text::Alignment::Center,
    )
    .draw(&mut display)
    .unwrap();

    if let Err(e) = epd {
        println!("Cannot write content to EPD: {:?}", e);
    } else {
        let _ = epd.and_then(|mut epd| {
            epd.clear_frame(&mut spi, &mut delay)?;
            epd.display_frame(&mut spi, &mut delay)?;
            delay.delay_ms(1000u32);
            epd.update_frame(&mut spi, &display.buffer(), &mut delay)?;
            epd.display_frame(&mut spi, &mut delay)?;
            epd.sleep(&mut spi, &mut delay)
        })
        .map_err(|err| {
            println!("error writing buffer to display: {:?}", err);
            err
        });
    }

    loop {
        println!("Hello Noah");
        block!(timer0.wait()).unwrap();
    }
}
