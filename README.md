## Quick usage

1. Add the ESP32C3 rust target: `rustup target add riscv32imc-unknown-none-elf`
2. Install [`cargo-embed`][cargo-embed]
3. Install the following udev rule under `/etc/udev/rules.d/`:
```
# Espressif dev kit FTDI
ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6010", MODE="666", TAG+="uaccess"

# Espressif USB JTAG/serial debug unit
ATTRS{idVendor}=="303a", ATTRS{idProduct}=="1001", MODE="666", TAG+="uaccess"

# Espressif USB Bridge
ATTRS{idVendor}=="303a", ATTRS{idProduct}=="1002", MODE="666", TAG+="uaccess"
```
4. Flash and watch the output with `cargo embed`


## ESP32 Pinout

### E-Paper Display

For Waveshare 2.9" displays:

| Waveshare I/O | ESP32 GPIO |
| :-----------: | :--------: |
| BUSY          | G4         |
| RST           | G2         |
| DC            | G3         |
| CS            | G5         |
| CLK           | G6         |
| DIN           | G7         |
| GND           | GND        |
| 3.3V          | 3V3        |


[cargo-embed]: https://github.com/probe-rs/probe-rs/tree/master/cargo-embed
