#!/usr/bin/env bash
#
# Define various aliases for containers. This script must be sourced to be
# useful.
$(return 1>/dev/null 2>&1)
if [[ $? -ne 0 ]]; then
    echo "This script must be sourced." 1>&2
    exit 1
fi


# Common variables
ARG_USER=(--userns keep-id --user "$(id -u):$(id -g)")
DEV_USB_NUM=0
REGISTRY="registry.gitlab.com/c8160/embedded-rust"

# Helper functions for dynamic evaluation

# Returns the root of the git project that $PWD is in. If $PWD is not within
# any git project, returns $PWD instead.
function __gitroot {
    GITROOT=$(git rev-parse --show-toplevel 2>/dev/null)
    if [[ $? -ne 0 ]]; then
        echo -e "\e[0;31m!! Not in a git repository. Mounting '$PWD' instead\e[0m" 1>&2
        pwd
    else
        echo $GITROOT
    fi
}

function _vol_pwd {
    echo -v "$PWD:$PWD:z" -w "$PWD"
}

function _vol_gitroot {
    GR=$(__gitroot)
    echo -v "${GR}:${GR}:z" -w "$PWD"
}

function _vol_gitroot_ro {
    GR=$(__gitroot)
    echo -v "${GR}:${GR}:ro" -w "$PWD"
}

function _vol_cargo {
    echo -v esp32-cargo-registry:/usr/local/share/cargo/registry:z
}

function _dev_ttyusb {
    TTYS=""
    DEV=$(ls /dev/)
    USB=$(echo $DEV | grep "ttyUSB")
    if [[ -n "$USB" ]]; then
        TTYS="$TTYS $(echo $USB | xargs printf "--device /dev/%s ")"
    fi
    ACM=$(echo $DEV | grep "ttyACM")
    if [[ -n "$ACM" ]]; then
        TTYS="$TTYS $(echo $ACM | xargs printf "--device /dev/%s ")"
    fi
    [[ -z "$TTYS" ]] && { \
        return 1; \
    }
    echo --security-opt label\=disable $TTYS
}

function _dev_bus_usb {
    echo --security-opt label\=disable -v /dev/bus/usb:/dev/bus/usb
}

function __disclaimer {
    YELLOW="\e[0;33m"
    RESET="\e[0m"
    echo "" 1>&2
    echo -e "${YELLOW}>>> Running '$1' via alias <<<" 1>&2
    echo "" 1>&2
    echo -en "$RESET" 1>&2
}

#function espflash {
#    __disclaimer "$0"
#    podman run --rm -it \
#        $(_dev_ttyusb) \
#        $(_vol_gitroot_ro) \
#        $REGISTRY/espflash:1.7.0 \
#        "$@"
#}

function espmonitor {
    __disclaimer "$0"
    podman run --rm -it \
        $(_dev_ttyusb) \
        $(_vol_gitroot_ro) \
        $REGISTRY/espmonitor:0.10.0 \
        "$@"
}

function espcargo {
    __disclaimer "$0"
    podman run --rm -it \
        $(_vol_gitroot) \
        $(_vol_cargo) \
        $REGISTRY/rust-xtensa32:1.66.0-nightly \
        /usr/share/cargo/bin/cargo "$@"
}

function esprust-analyzer {
    podman run --rm -i \
        $(_vol_gitroot) \
        $(_vol_cargo) \
        $REGISTRY/rust-xtensa32:1.66.0-nightly \
        /usr/local/bin/rust-analyzer "$@"
}

function espopenocd {
    __disclaimer "$0"
    podman run --rm -it \
        $(_vol_gitroot_ro) \
        $(_dev_bus_usb) \
        -p 3333:3333 \
        $REGISTRY/openocd-esp32:0.11.0 \
        "$@"
}

function espgdb {
    __disclaimer "$0"
    podman run --rm -it \
        $(_vol_gitroot_ro) \
        $(_vol_cargo) \
        --network host \
        $REGISTRY/gdb-xtensa32:esp-2021r2-patch3 \
        "$@"
}

# Add bin to PATH
export PATH="$(dirname $(readlink -f $0))/bin:$PATH"

function podalias {
    if [[ $# -gt 0 ]]; then
        case "$1" in
            "unset")
                unset -f __disclaimer
                unset -f __gitroot
                unset -f espflash
                unset -f espmonitor
                unset -f esprs
                unset -f espcargo
                unset -f espopenocd
                unset -f espgdb
                unset -f "$0"
                echo "Aliases removed"
                ;;
            *)
                echo "Unknown argument '$1'" 1>&2
                ;;
        esac
    fi
}

echo "Aliases have been sourced"
